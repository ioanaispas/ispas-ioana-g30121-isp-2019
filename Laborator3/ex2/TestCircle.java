package Laborator3.ex1.ex2;

public class TestCircle {
    public static void main(String[] args) {
        Circle c1 = new Circle();
        System.out.println("The first circle has the length of the radius: " + c1.getRadius());
        Circle c2 = new Circle();
        System.out.println("The second circle has the area: " + c2.getArea());
    }
}
