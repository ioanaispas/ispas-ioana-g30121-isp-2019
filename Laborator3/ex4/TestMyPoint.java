package Laborator3.ex4;

public class TestMyPoint {
    public static void main(String[] args) {
        MyPoint p1 = new MyPoint();
        MyPoint p2 = new MyPoint(3, 6);
        MyPoint p3 = new MyPoint(2, 5);
        MyPoint p4 = new MyPoint(4, 4);
        String a = p1.toString();
        String b = p2.toString();
        String c = p3.toString();
        String d = p4.toString();
        System.out.println("a=" + a);
        System.out.println("b=" + b);
        System.out.println("c=" + c);
        System.out.println("d=" + d);
        p1.setX(9);
        p4.setY(1);
        System.out.println("The new X coordinate for p1 is: " + p1.getX());
        System.out.println("The new Y coordinate for p4 is: " + p4.getY());
        p2.setXY(1, 2);
        System.out.println("The new coordinates for p2 are: (" + p2.getX() + ", " + p2.getY() + ")");
        System.out.println("The distance between p3 and (0,0) is: " + p3.distance(0, 0));
        System.out.println("The distance between p1 and p2 is:  " + p1.distance(p2));
    }
}
