package Laborator3.ex4;

public class MyPoint {
    public int x, y;

    public MyPoint() {
        this.x = 0;
        this.y = 0;
    }

    public MyPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setXY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public String toString() {
        String coordinates = "(" + Integer.toString(this.x) + ", " + Integer.toString(this.y) + ")";
        return coordinates;
    }

    public double distance(int x, int y) {
        return Math.sqrt((this.x - x) ^ 2 + (this.y - y) ^ 2);
    }

    public double distance(MyPoint p) {
        return Math.sqrt((this.x - p.x) ^ 2 + (this.y - p.y) ^ 2);
    }
}
