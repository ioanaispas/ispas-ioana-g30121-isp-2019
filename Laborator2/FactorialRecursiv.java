package Laborator2;

import java.util.Scanner;

public class FactorialRecursiv {

    public static int f(int n) {
        if (n > 1)
            return n * f(n - 1);
        else
            return 1;
    }

    public static void main(String[] args) {
        int n, fact = 1;
        Scanner in = new Scanner(System.in);
        System.out.print("Introduce the number n =  ");
        n = in.nextInt();
        if (n < 0) {
            System.out.print("Error");
            System.out.println();
        } else {
            fact = f(n);


            System.out.print("The factorial is : " + fact);
            System.out.println();
        }
    }
}