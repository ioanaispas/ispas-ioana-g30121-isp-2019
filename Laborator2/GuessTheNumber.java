package Laborator2;

import java.util.*;
import java.util.Scanner;

public class GuessTheNumber {
    public static void main(String[] args) {
        Random r = new Random();
        int a = r.nextInt(10);
        int c = 1;
        System.out.println("Guess the number! You only have 3 changes!");

        Scanner in = new Scanner(System.in);

        while (c <= 3) {
            System.out.println();
            int n1;
            System.out.println("Round " + c);
            System.out.println("Introduce a number =  ");
            n1 = in.nextInt();
            if (n1 == a) {
                System.out.println("You guessed the number");
                break;
            }
            if (n1 < a)
                System.out.println("Wrong answer, your number is too low");
            if (n1 > a)
                System.out.println("Wrong answer, your number is too high");

            c += 1;

            int n2;
            System.out.print("Round " + c);
            System.out.println();
            System.out.print("Introduce another number =  ");
            System.out.println();
            n2 = in.nextInt();
            if (n2 == a) {
                System.out.print("You guessed the number");
                break;
            }
            if (n2 < a)
                System.out.print("Wrong answer, your number is too low");
            if (n2 > a)
                System.out.print("Wrong answer, your number is too high");

            c = c + 1;

            System.out.println();

            int n3;
            System.out.println("Round " + c);
            System.out.println("It is your last chance!Introduce another number  =  ");
            n3 = in.nextInt();
            if (n3 == a) {
                System.out.print("You guessed the number");
                break;
            }
            if (n3 < a) {
                System.out.println("Wrong answer, your number is too low. You lost!");
                System.out.println("The number you should have guessed is " + a);
            }
            if (n3 > a) {
                System.out.println("Wrong answer, your number is too high. You lost!");
                System.out.print("The number you shuld have guessed is " + a);
            }
            c++;
        }
    }
}


