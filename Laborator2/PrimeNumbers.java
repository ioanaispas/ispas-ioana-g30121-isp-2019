package Laborator2;

import java.util.Scanner;

import static java.lang.StrictMath.sqrt;

public class PrimeNumbers {

    public static void main(String[] args) {
        int a;
        int b;
        Scanner in = new Scanner(System.in);
        System.out.println("Set the [a,b] interval");
        System.out.println("Choose a= ");
        a = in.nextInt();
        System.out.println("Choose b =  ");
        b = in.nextInt();
        int c = 0;
        int i;

        for (i = a; i <= b; i++) {
            int ok = 1;
            int d = 2;
            while (d <= sqrt(i)) {
                if (i % d == 0)
                    ok = 0;
                d = d + 1;
            }
            if (ok == 1) {
                System.out.println("The prime number you just found is " + i);
                c++;
            }
        }
        System.out.println();
        System.out.print("You have found " + c + " prime numbers");
    }
}
