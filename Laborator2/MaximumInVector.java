package Laborator2;

import java.util.Random;
import java.util.Scanner;

public class MaximumInVector {

    public static void main(String[] args) {
        int n, i, max;
        Scanner in = new Scanner(System.in);
        System.out.print("Introduce the length of the vector =  ");
        n = in.nextInt();
        int[] v = new int[n];
        for (i = 0; i < n; i++) {
            v[i] = new Random().nextInt(n);
            }
        System.out.print("Your vector is: ");
        for (i = 0; i < n; i++) {
            System.out.print(v[i]);
            System.out.print(',');
        }
        max = v[0];
        for (i = 1; i < n; i++) {
            if (v[i] > max)
                max = v[i];
        }
        System.out.println();
        System.out.print("The maximum number is:  " + max);
    }
}