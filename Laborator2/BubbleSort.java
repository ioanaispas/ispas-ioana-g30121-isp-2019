package Laborator2;

import java.util.Random;
import java.util.Scanner;

public class BubbleSort {
    public static void main(String[] args) {
        int n, i;
        Scanner in = new Scanner(System.in);
        System.out.print("Introduce the length of the vector =  ");
        n = in.nextInt();
        int[] v = new int[n];
        for (i = 0; i < n; i++) {
            v[i] = new Random().nextInt(n);
        }
        System.out.print("Your vector is: ");
        for (i = 0; i < n; i++) {
            System.out.print(v[i]);
            System.out.print(' ');
        }
        int ok;
        do {
            ok = 1;
            for (i = 0; i < n - 1; i++) {
                if (v[i] > v[i + 1]) {
                    int aux = v[i];
                    v[i] = v[i + 1];
                    v[i + 1] = aux;
                    ok = 0;
                }
            }
        }
        while (ok != 1);
        System.out.println();
        System.out.print("The sorted vector is : ");
        for (i = 0; i < n; i++) {
            System.out.print(v[i]);
            System.out.print(' ');
        }
    }
}
