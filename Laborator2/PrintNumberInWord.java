package Laborator2;

import java.util.Scanner;

public class PrintNumberInWord {

    public static void main(String[] args) {
        int a;
        String number = "Something";
        Scanner in = new Scanner(System.in);
        System.out.print("Introduce a number from 1 to 9 =  ");
        a = in.nextInt();

        switch (a) {
            case 1:
                number = "one";
                break;
            case 2:
                number = "two";
                break;
            case 3:
                number = "three";
                break;
            case 4:
                number = "four";
                break;
            case 5:
                number = "five";
                break;
            case 6:
                number = "six";
                break;
            case 7:
                number = "seven";
                break;
            case 8:
                number = "eight";
                break;
            case 9:
                number = "9ine";
                break;
        }
        System.out.println("The number you have chosen is " + number);
    }
}

