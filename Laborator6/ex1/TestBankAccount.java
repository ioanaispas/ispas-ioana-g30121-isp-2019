package Laborator6.ex1;

public class TestBankAccount {
    public static void main(String[] args) {

        BankAccount b1 = new BankAccount("Emilia", 200);
        BankAccount b2 = new BankAccount("George", 300);
        BankAccount b3 = new BankAccount("Dorin", 200);
        BankAccount b4 = new BankAccount("Mihaela", 450);

        if (b1.equals(b3)){
            System.out.println(b1 + " and " +b3+ " are equals");
        }
        else
        {
            System.out.println(b1 + " and " +b3+ " are not equals");
        }
        if (b2.equals(b4)){
            System.out.println(b2 + " and " +b4+  "are equals");
        }
        else
        {
            System.out.println(b2 + " and " +b4+ " are not equals");
        }
        System.out.println(b1.hashCode());
        System.out.println(b4.hashCode());

    }

}
