package Laborator6.ex4;

import java.util.*;

import static java.lang.System.*;

public class Dictionary {
    HashMap dct = new HashMap();

    public void addWord(Word w, Definition d) {
        if (dct.containsKey(w))
            out.println("Modify the current word");
        else
            out.println("Add a new word");
        dct.put(w, d);
    }

    public Object getDefinition(Word w) {
        out.println("Cauta " + w);
        out.println(dct.containsKey(w));
        return dct.get(w);
    }

    public void printDictionary() {
        out.println(dct);
    }

    public void getAllWords() {

        for (Object word : dct.keySet()) System.out.println(word.toString());
    }

    public void getAllDefinitions() {

        for (Object definition : dct.values()) System.out.println(definition.toString());
    }
}
