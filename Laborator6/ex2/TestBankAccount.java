package Laborator6.ex2;

public class TestBankAccount {
    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.addAccount("Emilia", 200);
        bank.addAccount("George", 300);
        bank.addAccount("Dorin", 200);
        bank.addAccount("Mihaela", 450);
        bank.addAccount("Marius",600);
        bank.addAccount("Diana",300);
        System.out.println("Print Accounts by balance");
        bank.printAccounts();
        System.out.println("Print Accounts between the following limits: [200,450]");
        bank.printAccounts(200,450);
        System.out.println("Get Account by owner name");
        bank.getAccount("Diana");
        System.out.println("Get All Account ordered by name");
        bank.getAllAccount();

    }
}
