package Laborator6.ex3;

public class TestBankAccount {
    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.addAccount("Emilia", 200);
        bank.addAccount("George", 320);
        bank.addAccount("Dorin", 270);
        bank.addAccount("Mihaela", 450);
        bank.addAccount("Marius",100);
        bank.addAccount("Diana",300);
        System.out.println("Print Accounts by balance");
        bank.printAccounts();
        System.out.println("Print Accounts between the following limits: [200,450]");
        bank.printAccounts(200,450);
        System.out.println("Get Account by owner name");
        bank.getAccount("Emilia",200);
        System.out.println("Get All Account ordered by name");
        bank.getAllAccount();

    }
}
