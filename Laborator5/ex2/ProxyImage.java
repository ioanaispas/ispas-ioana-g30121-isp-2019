package Laborator5.ex2;

public class ProxyImage implements Image{

    private RealImage realImage;
    private RotatedImage rotatedImage;
    private String fileName;

    public ProxyImage(String fileName){
        this.fileName = fileName;
    }

    @Override
    public void display() {
        if(realImage == null){
            realImage = new RealImage(fileName);
        }
        realImage.display();
        if(rotatedImage == null){
            rotatedImage = new RotatedImage(fileName);
        }
        rotatedImage.display();
    }
}
