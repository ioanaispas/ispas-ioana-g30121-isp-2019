package Laborator5.ex1;

public class TestShape {
    public static void main(String[] args) {
        Circle c1= new Circle(3,"yellow",true);
        System.out.println("Circle c1: "+ c1.toString());
        System.out.println("The area of circle c1 is: "+c1.getArea());
        System.out.println("The perimeter of circle c1 is: "+c1.getPerimeter());
        Rectangle r1 = new Rectangle(3,6,"black",false);
        System.out.println("Rectangle r1: "+ r1.toString());
        System.out.println("The area of rectangle r1 is: "+r1.getArea());
        System.out.println("The perimeter of rectangle r1 is: "+r1.getPerimeter());
        Square s1 = new Square(8,"pink",true);
        System.out.println("Square s1: "+ s1.toString());
        System.out.println("The area of square s1 is: "+s1.getArea());
        System.out.println("The perimeter of square s1 is: "+s1.getPerimeter());
    }

}
