package Laborator5.ex1;

public class Circle extends Shape {
    protected double radius;

    public Circle() {

    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(double radius, String color, boolean filled) {
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea() {
        // TODO Auto-generated method stub
        return this.radius*this.radius * (Math.PI);
    }

    @Override
    public double getPerimeter() {
        // TODO Auto-generated method stub
        return 2 * this.radius * Math.PI;
    }

    @Override
    public String toString() {
        return "Circle [radius=" + this.radius + "]"+ " which is a subclass of " + super.toString();
    }

}