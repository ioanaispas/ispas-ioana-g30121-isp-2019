package Laborator5.ex1;
public class Rectangle extends Shape {
    protected double width;
    protected double length;

    public Rectangle() {

    }

    public Rectangle(double width, double length) {
        super();
        this.width = width;
        this.length = length;
    }

    public Rectangle(double width, double length, String color, boolean filled) {
        super(color, filled);
        this.width = width;
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    @Override
    public double getArea() {
        // TODO Auto-generated method stub
        return this.length * this.width;
    }

    @Override
    public double getPerimeter() {
        // TODO Auto-generated method stub
        return 2 * this.length + 2 * this.width;
    }

    @Override
    public String toString() {
        return "Retangle [width=" + this.width + ", length=" + this.length + "] which is a subclass of " + super.toString();
    }
}