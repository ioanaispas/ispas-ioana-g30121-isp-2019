package Laborator5.ex3;

import java.util.*;
import java.util.Scanner;
public class LightSensor extends Sensor{
    @Override
    public int readValue() {
        Random r = new Random();
        return r.nextInt(100);
    }
    @Override
    public String getLocation() {
        return null;
    }
}
