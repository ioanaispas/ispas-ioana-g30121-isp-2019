package Laborator4.ex6;

public class TestShape {
    public static void main(String[] args) {
        Shape s = new Shape("green",true);
        System.out.println(s.toString());

        Circle c = new Circle(3,"yellow",true);
        System.out.println(c.toString());
        System.out.println("Area = "+c.getArea());
        System.out.println("Perimeter = "+c.getPerimeter());
        System.out.println("Is filled = "+c.isFilled());

        Rectangle r = new Rectangle(4,6,"pink",false);
        System.out.println(r.toString());
        System.out.println("Width = "+r.getWidth());
        r.setWidth(5);
        System.out.println("New width = "+r.getWidth());
        System.out.println("Area = "+r.getArea());
        System.out.println("Is filled = "+r.isFilled());

        Square sq = new Square(10,"turquoise",false);
        sq.setSide(3);
        System.out.println(sq.toString());
        System.out.println("Is filled = "+sq.isFilled());
        System.out.println("Perimeter = "+sq.getPerimeter());


    }
}