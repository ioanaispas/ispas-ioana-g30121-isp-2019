package Laborator4.ex4;

import Laborator4.ex2.Author;
import Laborator4.ex4.Book;

public class TestBook {
    public static void main(String[] args) {
        Author a1 = new Author("John Green", "johngreen@gmail.com", 'm');
        Author a2 = new Author("Maureen Johnson", "maureenjohnson@gmail.com", 'f');
        Author a3 = new Author("Lauren Myracle", "laurenmyracle@gmail.com", 'f');
        Author a4 = new Author("Stephanie Dray", "stephaniedray@yahoo.com", 'f');
        Author a5 = new Author("Vicky Alvear", "vickyalvear@yahoo.com", 'm');
        Author[] authors = new Author[3];
        authors[0] = a1;
        authors[1] = a2;
        authors[2] = a3;
        Book b1 = new Book("Let it snow", authors, 30);
        authors = new Author[2];
        authors[0] = a4;
        authors[1] = a5;
        Book b2 = new Book("A day of fire", authors, 26, 100);
        System.out.println("The second book is " + b2.getName());
        System.out.println("Authors: ");
        b2.printAuthors();
        System.out.println("The price of the first book is " + b1.getPrice() + " dollars");
        System.out.println("There are " + b1.getQtyInStock() + " Let it snow books in stock");
        b2.setPrice(13);
        System.out.println("The new price of the second book is " + b2.getPrice() + " dollars");
        b2.setQtyInStock(90);
        System.out.println("There are only " + b2.getQtyInStock() + " A day of fire books in stock!");
        System.out.println("The information about the first book:", b1.toString());
    }
}
