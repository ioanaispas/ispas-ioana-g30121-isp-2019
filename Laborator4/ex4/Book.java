package Laborator4.ex4;

import Laborator4.ex2.Author;

public class Book {
    private String name;
    private Author[] authors;
    private double price;
    private int qtyInStock = 0;

    public Book(String name, Author[] authors, double price) {
        this.name = name;
        this.authors = authors;
        this.price = price;
    }

    public Book(String name, Author[] authors, double price, int qtyInStock) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName() {

        return name;
    }

    public Author[] getAuthors() {
        return authors;
    }

    public double getPrice() {

        return price;
    }

    public void setPrice(double price)
    {
        this.price = price;
    }

    public int getQtyInStock() {

        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    @Override
    public String toString() {
        String v = this.getName() + " by " + authors.length + " authors ";
        return v;
    }

    public void printAuthors() {
        int i;
        for (i = 0; i<=authors.length; i++){
            System.out.println(this.authors[i].toString());
        }
    }
}
