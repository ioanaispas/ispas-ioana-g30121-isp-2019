package Laborator4.ex3;

import Laborator4.ex2.Author;
import Laborator4.ex3.Book;
public class TestBook {
    public static void main(String[] args) {
        Author a1 = new Author("Anna Todd", "annatodd@yahoo.com", 'f');
        Author a2 = new Author("Suzzane Collins", "suzzanecollins@yahoo.com", 'f');
        Author a3 = new Author("Veronica Roth", "veronicaroth@yahoo.com", 'f');
        Book b1 = new Book("After", a1, 25, 150);
        Book b2 = new Book("Hunger Games", a2, 15, 110);
        Book b3 = new Book("Divergent", a3, 20, 200);
        System.out.println("The author of the book After is " + b1.getAuthor());
        System.out.println("The second book is " + b2.getName());
        System.out.println("The price of the third book is " + b3.getPrice() + " dollars");
        System.out.println("There are " + b1.getQtyInStock() + " After books in stock");
        b2.setPrice(13);
        System.out.println("The new price of the Hunger Games book is " + b2.getPrice() + " dollars");
        b3.setQtyInStock(90);
        System.out.println("There are only " + b3.getQtyInStock() + " Divergent books in stock! Don't miss your chance to buy one!");
        System.out.println("The information about the third book is " + b3.toString());
    }
}
