package Laborator4.ex5;

import Laborator4.ex5.Cylinder;

public class TestCylinder {
    public static void main(String args[]) {
        Cylinder c1 = new Cylinder(2, 8);
        Cylinder c2 = new Cylinder(1, 5);
        Cylinder c3 = new Cylinder(4, 2);
        System.out.println("Area of c1 = " + c1.getArea());
        System.out.println("Area of c2 = " + c2.getArea());
        System.out.println("Area of c3 = " + c3.getArea());
    }
}
}
