package Laborator4.ex5;

import Laborator4.ex1.Circle;

    public class Cylinder extends Circle {
        private double height;
        private double volume;

        public Cylinder() {
            this.height = 1.0;
        }

        public Cylinder(double radius) {
            super(radius);
        }

        public Cylinder(double radius, double height) {
            super(radius);
            this.height = height;
        }

        public double getHeight() {
            return this.height;
        }

        @Override
        public double getArea() {
            double a=this.getRadius() * this.getRadius() * 3.14 * this.height;
            return a;
        }
    }

