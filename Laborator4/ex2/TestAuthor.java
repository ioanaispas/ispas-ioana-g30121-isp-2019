package Laborator3.ex3;

public class TestAuthor {
    public static void main(String[] args) {
        Author a1 = new Author("Ana Popescu", "anapopescu@yahoo.com", 'f');
        Author a2 = new Author("Alexandru Rus", "alexandru.rus@yahoo.com",'m');
        Author a3 = new Author("Maria Campean", "mariacampean12@yahoo.com", 'f');
        System.out.println("The name of the first author is "+ a1.getName());
        System.out.println("The gender of the second author is "+ a2.getGender());
        System.out.println("The email of the third author is "+ a3.getEmail());
        String s = a1.toString();
        System.out.println("First author: "+s);
        a2.setEmail("alexandru_rus@gmail.com");
        System.out.println("The new email of the second author is "+a2.getEmail());
    }
}
