package Laborator10.ex3;

public class Counter2 extends Thread {

    Counter2(String name) {
        super(name);
    }

    public void run() {
        for (int i = 100; i <= 200; i++) {
            System.out.println(getName() + " i = " + i);
            try {
                Thread.sleep((int) (Math.random() * 1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(getName() + " job finalised.");
    }
}
