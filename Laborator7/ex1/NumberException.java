package Laborator7.ex1;

public class NumberException extends Exception {
    int n;

    public NumberException(int n, String msg) {
        super(msg);
        this.n = n;
    }

    int getNumber() {
        return n;
    }
}