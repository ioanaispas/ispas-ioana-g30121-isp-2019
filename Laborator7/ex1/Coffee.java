package Laborator7.ex1;

public class Coffee {
    private int temp;
    private int conc;
    private int pred;

    Coffee(int t, int c, int n) {
        temp = t;
        conc = c;
        pred = n;

    }

    int getTemp() {
        return temp;
    }

    int getConc() {
        return conc;
    }

    int getPred() {
        return pred;
    }

    public String toString() {
        return "[cofee temperature=" + temp + ":concentration=" + conc + " predefined number of coffes: " + pred + "]";
    }
}
