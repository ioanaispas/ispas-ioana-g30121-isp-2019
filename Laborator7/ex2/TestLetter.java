package Laborator7.ex2;

import java.nio.file.Path;
import java.nio.file.Paths;

public class TestLetter {
    public static void main(String[] args) {
        Path file = Paths.get("C:/Users/Ispas Ioana/IdeaProjects/untitled/src/Laborator7/ex2/data.txt");
        CountLetter cl = new CountLetter(file, 'a');
        CountLetter c2 = new CountLetter(file, 'r');
        CountLetter c3 = new CountLetter(file, 't');
        System.out.println("letter a: " + cl.count());
        System.out.println("letter r:  " + c2.count());
        System.out.println("letter o: " + c3.count());

    }
}
